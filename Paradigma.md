# PARADIGMA DE LA PROGRAMACIÓN


## ¿Que es?

>*Són un conjunto de reglas para delimitar **tipos de programación según el tipo de problema** o situacion planteada.*


|Tipos de<br>programación  | Imperativa | Declarativa | Orientada a<br>objetos  |
| :------- | :------------: | :----------------: | :----------------: |
| Su uso  | Dar<br>instrucciones         | Pedir<br>resultado              | Clasificar              |

## Programación Imperativa

Dar instrucciones de como hacerlo.

![](https://upload.wikimedia.org/wikipedia/commons/0/0b/PET-basic.png)

## Programación Declarativa

- Estilo de construcción de la estructura y de los elementos de un programa.
- Indicar lo que quiero hacer.
- Sin dar instrucciones para llevarlas a cabo.
- Cambiar estados de un programa mediante declaraciones.
- A menudo trata a los programas como teorias de una forma lógica.
- Simplifica mucho programar otros programas en paralelo.
- Se puede entender como estilo de programación declarativa toda aquella que no sea imperativa.
- Programa de alto nivel que describe lo que debe hacer un cálculo.
- Es un programa que no tiene efectos colaterales.
- Reusable.

```html
<img src="./imagen.jpg"/>
```
![](https://cdn-images-1.medium.com/max/2000/1*UwF1vVdWK5toYw1EHEz7pw.png)


## Programación Orientada a objetos

![](https://blog.carreralinux.com.ar/wp-content/uploads/2017/03/programacion-orientada-a-objetos-1.png)
